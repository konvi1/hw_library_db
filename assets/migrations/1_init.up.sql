create table departments(
    id uuid not null,
    department_name varchar(256) not null,
    department_code varchar(20) not null,
    active bool not null,
    created_at        timestamp     not null,
    updated_at        timestamp     not null,
    primary key (id)
);

create table positions(
    id uuid not null,
    position_name varchar(256) not null,
    position_code varchar(20) not null,
    created_at        timestamp     not null,
    updated_at        timestamp     not null,
    primary key (id)
);

create table users(
    id uuid not null,
    user_surname varchar(50) not null,
    user_firstname varchar(50) not null,
    user_secondname varchar(50),
    birthday  timestamp     not null,
    department_id uuid,
    position_id uuid,
    active bool not null,
    created_at        timestamp     not null,
    updated_at        timestamp     not null,
    primary key (id)
);

alter table users add constraint users_to_departments foreign key (department_id) references departments(id);
alter table users add constraint users_to_positions foreign key (position_id) references positions(id)
