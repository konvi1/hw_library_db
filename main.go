package main

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq"
)

type director struct {
	ID   int
	Name string
}

const (
	host     = "localhost"
	port     = 5433
	user     = "postgres"
	password = "test"
	dbname   = "postgres"
)

func main() {
	// Строка подключения к БД из
	// переменной окружения.
	/*	connstr := os.Getenv("moviesdb")
		if connstr == "" {
			log.Fatal("не указан адрес подключения к БД")
		}
	*/
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)
	// Подключение к БД.
	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		log.Fatal(err)
	}

	// Не забываем освобождать ресурсы.
	defer db.Close()

	// Создаем таблицы
	/*	qCreateTable := `CREATE TABLE directors(
			id SERIAL PRIMARY KEY,
			name TEXT NOT NULL
		  );`
		result, err := db.Exec(qCreateTable)
		if err != nil {
			log.Fatal(err)
		}
	*/
	// Добавляем запись в БД.
	result, err := newDirector(db, director{Name: "James Cameron"})
	if err != nil {
		log.Fatal(err)
	}

	// При изменении данных можно получить обратную связь от БД.
	n, _ := result.RowsAffected()
	fmt.Printf("Вставлено записей: %d\n", n)

	// Запрашиваем все записи из таблицы.
	data, err := directors(db)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Режиссеры в БД:\n%+v", data)
}

// newDirector добавляет запись в таблицу режиссеров.
func newDirector(db *sql.DB, d director) (sql.Result, error) {
	query := `INSERT INTO directors (name) VALUES ($1);`
	return db.Exec(query, d.Name)
}

// directors возвращает список всех режиссеров.
func directors(db *sql.DB) ([]director, error) {
	query := `SELECT id, name FROM directors;`
	rows, err := db.Query(query)
	if err != nil {
		return nil, err
	}
	var directors []director
	for rows.Next() {
		var d director
		err = rows.Scan(
			&d.ID,
			&d.Name,
		)
		if err != nil {
			return nil, err
		}
		directors = append(directors, d)
	}
	return directors, rows.Err()
}
