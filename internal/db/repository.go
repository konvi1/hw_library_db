package db

import (
	"context"
	"log"

	"github.com/go-pg/pg/v9"
	"github.com/go-pg/pg/v9/orm"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type Storage struct {
	db *pg.DB
}

func NewStorageFactory(db *pg.DB) *Storage {
	return &Storage{
		db: db,
	}
}

func (f *Storage) NewRepository() Repository {
	return Repository{
		db: f.db,
	}
}

func (f *Storage) NewTransaction(ctx context.Context) (RepositoryTx, error) {
	tx, err := f.db.WithContext(ctx).Begin()
	if err != nil {
		log.Println("unable to begin transaction:", err)
		return RepositoryTx{}, status.Error(codes.Internal, "unable to begin transaction")
	}

	return RepositoryTx{
		tx: tx,
		Repository: Repository{
			db: tx,
		},
	}, nil
}

type Repository struct {
	db orm.DB
}

type RepositoryTx struct {
	tx *pg.Tx
	Repository
}

func (c RepositoryTx) Commit(ctx context.Context) error {
	err := c.tx.Commit()
	if err != nil {
		log.Println("unable to begin transaction:", err)
		return status.Error(codes.Internal, "unable to commit transaction")
	}

	return nil
}

func (c RepositoryTx) Rollback(ctx context.Context) {
	err := c.tx.Rollback()
	if err != nil {
		log.Println("unable to begin transaction:", err)
	}
}
