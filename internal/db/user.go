package db

import (
	"context"
	"log"

	"github.com/go-pg/pg/v9"
	"gitlab.com/konvi1/hw_library_db/internal/db/model"
	"gitlab.com/konvi1/hw_library_db/internal/library/entity"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func modelToUser(r *model.User) *entity.User {
	return &entity.User{
		Id:             r.ID,
		UserSurname:    r.UserSurname,
		UserFirstname:  r.UserFirstname,
		UserSecondname: r.UserSecondname,
		Birthday:       r.Birthday,
		DepartmentID:   r.DepartmentID,
		PositionID:     r.PositionID,
		Active:         r.Active,
	}
}

func modelToUsers(r []*model.User) []*entity.User {
	res := make([]*entity.User, 0, len(r))
	for i := range r {
		res = append(res, modelToUser(r[i]))
	}
	return res
}

func (c Repository) CreateUser(ctx context.Context, r *entity.User) (*entity.User, error) {
	user := &model.User{
		UserSurname:    r.UserSurname,
		UserFirstname:  r.UserFirstname,
		UserSecondname: r.UserSecondname,
		Birthday:       r.Birthday,
		DepartmentID:   r.DepartmentID,
		PositionID:     r.PositionID,
		Active:         r.Active,
	}

	_, err := c.db.ModelContext(ctx, user).
		Insert()
	if err != nil {
		log.Println("unable to insert user")
		return nil, status.Error(codes.Internal, "unable to insert user")
	}

	r.Id = user.ID

	return r, nil
}

func (c Repository) UpdateUser(ctx context.Context, r *entity.User) error {
	if !model.IsValidUUID(r.Id) {
		return status.Error(codes.InvalidArgument, "invalid id")
	}

	user := model.User{
		ID:             r.Id,
		UserSurname:    r.UserSurname,
		UserFirstname:  r.UserFirstname,
		UserSecondname: r.UserSecondname,
		Birthday:       r.Birthday,
		DepartmentID:   r.DepartmentID,
		PositionID:     r.PositionID,
		Active:         r.Active,
	}

	_, err := c.db.ModelContext(ctx, &user).
		WherePK().
		Update()
	if err != nil {
		log.Println("unable to update user", err)
		return status.Error(codes.InvalidArgument, "unable to update user")
	}

	return nil
}

func (c Repository) GetUser(ctx context.Context, id string) (*entity.User, error) {
	if !model.IsValidUUID(id) {
		return nil, status.Error(codes.InvalidArgument, "invalid id")
	}

	var user model.User
	err := c.db.ModelContext(ctx, &user).
		Where(model.Columns.User.ID+" = ?", id).
		Select()
	if err != nil {
		if err == pg.ErrNoRows {
			log.Println("user not found")
			return nil, status.Error(codes.NotFound, "user not found")
		}
		log.Println("unable to select user")
		return nil, status.Error(codes.InvalidArgument, "unable to select user")
	}

	return modelToUser(&user), nil
}

func (c Repository) ListUsers(ctx context.Context) ([]*entity.User, error) {
	var users []*model.User
	err := c.db.ModelContext(ctx, &users).
		Select()
	if err != nil {
		log.Println("unable to select users")
		return nil, status.Error(codes.InvalidArgument, "unable to select users")
	}

	return modelToUsers(users), nil
}

func (c Repository) DeleteUser(ctx context.Context, id string) error {
	if !model.IsValidUUID(id) {
		return status.Error(codes.InvalidArgument, "invalid id")
	}

	_, err := c.db.ModelContext(ctx, (*model.User)(nil)).
		Where(model.Columns.User.ID+" = ?", id).
		Delete()
	if err != nil {
		log.Println("unable to delete user")
		return status.Error(codes.Internal, "unable to delete user")
	}

	return nil
}
