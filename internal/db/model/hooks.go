package model

import (
	"context"
	"time"

	"github.com/go-pg/pg/v9/orm"
)

var _ orm.BeforeInsertHook = (*Department)(nil)
var _ orm.BeforeUpdateHook = (*Department)(nil)

func (r *Department) BeforeInsert(ctx context.Context) (context.Context, error) {
	r.ID = GenStringUUID()

	date := time.Now()
	r.CreatedAt = date
	r.UpdatedAt = date

	return ctx, nil
}

func (r *Department) BeforeUpdate(ctx context.Context) (context.Context, error) {
	date := time.Now()
	r.UpdatedAt = date

	return ctx, nil
}

var _ orm.BeforeInsertHook = (*Position)(nil)
var _ orm.BeforeUpdateHook = (*Position)(nil)

func (r *Position) BeforeInsert(ctx context.Context) (context.Context, error) {
	r.ID = GenStringUUID()

	date := time.Now()
	r.CreatedAt = date
	r.UpdatedAt = date

	return ctx, nil
}

func (r *Position) BeforeUpdate(ctx context.Context) (context.Context, error) {
	date := time.Now()
	r.UpdatedAt = date

	return ctx, nil
}

var _ orm.BeforeInsertHook = (*User)(nil)
var _ orm.BeforeUpdateHook = (*User)(nil)

func (r *User) BeforeInsert(ctx context.Context) (context.Context, error) {
	r.ID = GenStringUUID()

	date := time.Now()
	r.CreatedAt = date
	r.UpdatedAt = date

	return ctx, nil
}

func (r *User) BeforeUpdate(ctx context.Context) (context.Context, error) {
	date := time.Now()
	r.UpdatedAt = date

	return ctx, nil
}
