//nolint
//lint:file-ignore U1000 ignore unused code, it's generated
package model

import (
	"time"
)

type ColumnsDepartment struct {
	ID, DepartmentName, DepartmentCode, Active, CreatedAt, UpdatedAt string
}

type ColumnsPosition struct {
	ID, PositionName, PositionCode, CreatedAt, UpdatedAt string
}

type ColumnsSchemaMigration struct {
	ID, Dirty string
}

type ColumnsUser struct {
	ID, UserSurname, UserFirstname, UserSecondname, Birthday, DepartmentID, PositionID, Active, CreatedAt, UpdatedAt string
	Department, Position                                                                                             string
}

type ColumnsSt struct {
	Department      ColumnsDepartment
	Position        ColumnsPosition
	SchemaMigration ColumnsSchemaMigration
	User            ColumnsUser
}

var Columns = ColumnsSt{
	Department: ColumnsDepartment{
		ID:             "id",
		DepartmentName: "department_name",
		DepartmentCode: "department_code",
		Active:         "active",
		CreatedAt:      "created_at",
		UpdatedAt:      "updated_at",
	},
	Position: ColumnsPosition{
		ID:           "id",
		PositionName: "position_name",
		PositionCode: "position_code",
		CreatedAt:    "created_at",
		UpdatedAt:    "updated_at",
	},
	SchemaMigration: ColumnsSchemaMigration{
		ID:    "version",
		Dirty: "dirty",
	},
	User: ColumnsUser{
		ID:             "id",
		UserSurname:    "user_surname",
		UserFirstname:  "user_firstname",
		UserSecondname: "user_secondname",
		Birthday:       "birthday",
		DepartmentID:   "department_id",
		PositionID:     "position_id",
		Active:         "active",
		CreatedAt:      "created_at",
		UpdatedAt:      "updated_at",

		Department: "Department",
		Position:   "Position",
	},
}

type TableDepartment struct {
	Name, Alias string
}

type TablePosition struct {
	Name, Alias string
}

type TableSchemaMigration struct {
	Name, Alias string
}

type TableUser struct {
	Name, Alias string
}

type TablesSt struct {
	Department      TableDepartment
	Position        TablePosition
	SchemaMigration TableSchemaMigration
	User            TableUser
}

var Tables = TablesSt{
	Department: TableDepartment{
		Name:  "departments",
		Alias: "t",
	},
	Position: TablePosition{
		Name:  "positions",
		Alias: "t",
	},
	SchemaMigration: TableSchemaMigration{
		Name:  "schema_migrations",
		Alias: "t",
	},
	User: TableUser{
		Name:  "users",
		Alias: "t",
	},
}

type Department struct {
	tableName struct{} `pg:"departments,alias:t,discard_unknown_columns"`

	ID             string    `pg:"id,pk,type:uuid"`
	DepartmentName string    `pg:"department_name,use_zero"`
	DepartmentCode string    `pg:"department_code,use_zero"`
	Active         bool      `pg:"active,use_zero"`
	CreatedAt      time.Time `pg:"created_at,use_zero"`
	UpdatedAt      time.Time `pg:"updated_at,use_zero"`
}

type Position struct {
	tableName struct{} `pg:"positions,alias:t,discard_unknown_columns"`

	ID           string    `pg:"id,pk,type:uuid"`
	PositionName string    `pg:"position_name,use_zero"`
	PositionCode string    `pg:"position_code,use_zero"`
	CreatedAt    time.Time `pg:"created_at,use_zero"`
	UpdatedAt    time.Time `pg:"updated_at,use_zero"`
}

type SchemaMigration struct {
	tableName struct{} `pg:"schema_migrations,alias:t,discard_unknown_columns"`

	ID    int64 `pg:"version,pk"`
	Dirty bool  `pg:"dirty,use_zero"`
}

type User struct {
	tableName struct{} `pg:"users,alias:t,discard_unknown_columns"`

	ID             string    `pg:"id,pk,type:uuid"`
	UserSurname    string    `pg:"user_surname,use_zero"`
	UserFirstname  string    `pg:"user_firstname,use_zero"`
	UserSecondname *string   `pg:"user_secondname"`
	Birthday       time.Time `pg:"birthday,use_zero"`
	DepartmentID   *string   `pg:"department_id,type:uuid"`
	PositionID     *string   `pg:"position_id,type:uuid"`
	Active         bool      `pg:"active,use_zero"`
	CreatedAt      time.Time `pg:"created_at,use_zero"`
	UpdatedAt      time.Time `pg:"updated_at,use_zero"`

	Department *Department `pg:"fk:department_id,rel:has-one"`
	Position   *Position   `pg:"fk:position_id,rel:has-one"`
}
