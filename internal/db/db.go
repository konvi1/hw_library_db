package db

import (
	"context"
	"log"
	"os"
	"strings"

	"github.com/go-pg/pg/v9"
	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/pkg/errors"
)

type dbLogger struct{}

func (d dbLogger) BeforeQuery(ctx context.Context, q *pg.QueryEvent) (context.Context, error) {
	return ctx, nil
}

func (d dbLogger) AfterQuery(ctx context.Context, q *pg.QueryEvent) error {
	query, err := q.FormattedQuery()
	if err != nil {
		log.Println(err)
	}
	log.Println(query)
	return nil
}

func NewDBConnection(ctx context.Context, config *pg.Options, migrations *MigrationConfig) (*pg.DB, error) {
	if strings.HasPrefix(config.Password, "$") {
		config.Password = strings.TrimPrefix(config.Password, "$")

		if os.Getenv(config.Password) != "" {
			config.Password = os.Getenv(config.Password)
		}
	}

	// migrate database
	err := Migrate(config, migrations)
	if err != nil {
		if err != migrate.ErrNoChange && err != migrate.ErrNilVersion {
			return nil, errors.WithMessage(err, "unable to migrate db")
		} else {
			log.Println("no new migrations found")
		}
	} else {
		log.Println("all migrations was executed correctly")
	}

	db := pg.Connect(config)
	db.AddQueryHook(dbLogger{})

	_, err = db.ExecContext(ctx, "SELECT 1")
	if err != nil {
		return nil, errors.Wrap(err, "cannot ping Postgres")
	}

	return db, nil
}

type MigrationConfig struct {
	Path string `yaml:"path"`
}

func Migrate(dbConfig *pg.Options, migrationConfig *MigrationConfig) (err error) {
	defer func() {
		if r := recover(); r != nil {
			err, _ = r.(error)
		}
	}()

	connectionString := "postgres://" + dbConfig.User + ":" + dbConfig.Password +
		"@" + dbConfig.Addr + "/" + dbConfig.Database + "?sslmode=disable"
	m, err := migrate.New(migrationConfig.Path, connectionString)
	if err != nil {
		return errors.New(err.Error())
	}

	defer func() {
		sourceErr, dbErr := m.Close()
		if sourceErr != nil {
			err = sourceErr
		}
		if dbErr != nil {
			err = dbErr
		}
	}()

	if err := m.Up(); err != nil {
		return err
	}

	return nil
}
