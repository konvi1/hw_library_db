package db

import (
	"context"
	"log"

	"github.com/go-pg/pg/v9"
	"gitlab.com/konvi1/hw_library_db/internal/db/model"
	"gitlab.com/konvi1/hw_library_db/internal/library/entity"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func modelToPosition(r *model.Position) *entity.Position {
	return &entity.Position{
		Id:           r.ID,
		PositionName: r.PositionName,
		PositionCode: r.PositionCode,
	}
}

func modelToPositions(r []*model.Position) []*entity.Position {
	res := make([]*entity.Position, 0, len(r))
	for i := range r {
		res = append(res, modelToPosition(r[i]))
	}
	return res
}

func (c Repository) CreatePosition(ctx context.Context, r *entity.Position) (*entity.Position, error) {
	position := &model.Position{
		PositionName: r.PositionName,
		PositionCode: r.PositionCode,
	}

	_, err := c.db.ModelContext(ctx, position).
		Insert()
	if err != nil {
		log.Println("unable to insert position")
		return nil, status.Error(codes.Internal, "unable to insert position")
	}

	r.Id = position.ID

	return r, nil
}

func (c Repository) UpdatePosition(ctx context.Context, r *entity.Position) error {
	if !model.IsValidUUID(r.Id) {
		return status.Error(codes.InvalidArgument, "invalid id")
	}

	position := model.Position{
		ID:           r.Id,
		PositionName: r.PositionName,
		PositionCode: r.PositionCode,
	}

	_, err := c.db.ModelContext(ctx, &position).
		WherePK().
		Update()
	if err != nil {
		log.Println("unable to update position", err)
		return status.Error(codes.InvalidArgument, "unable to update position")
	}

	return nil
}

func (c Repository) GetPosition(ctx context.Context, id string) (*entity.Position, error) {
	if !model.IsValidUUID(id) {
		return nil, status.Error(codes.InvalidArgument, "invalid id")
	}

	var position model.Position
	err := c.db.ModelContext(ctx, &position).
		Where(model.Columns.Position.ID+" = ?", id).
		Select()
	if err != nil {
		if err == pg.ErrNoRows {
			log.Println("position not found")
			return nil, status.Error(codes.NotFound, "position not found")
		}
		log.Println("unable to select position")
		return nil, status.Error(codes.InvalidArgument, "unable to select position")
	}

	return modelToPosition(&position), nil
}

func (c Repository) ListPositions(ctx context.Context) ([]*entity.Position, error) {
	var positions []*model.Position
	err := c.db.ModelContext(ctx, &positions).
		Select()
	if err != nil {
		log.Println("unable to select positions")
		return nil, status.Error(codes.InvalidArgument, "unable to select positions")
	}

	return modelToPositions(positions), nil
}

func (c Repository) DeletePosition(ctx context.Context, id string) error {
	if !model.IsValidUUID(id) {
		return status.Error(codes.InvalidArgument, "invalid id")
	}

	// проставить в null поле r.PositionID у всех пользователей
	// и статус проставить неактивный

	_, err := c.db.ModelContext(ctx, (*model.Position)(nil)).
		Where(model.Columns.Position.ID+" = ?", id).
		Delete()
	if err != nil {
		log.Println("unable to delete position")
		return status.Error(codes.Internal, "unable to delete position")
	}

	return nil
}
