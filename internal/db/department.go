package db

import (
	"context"
	"log"

	"github.com/go-pg/pg/v9"
	"gitlab.com/konvi1/hw_library_db/internal/db/model"
	"gitlab.com/konvi1/hw_library_db/internal/library/entity"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func modelToDepartment(r *model.Department) *entity.Department {
	return &entity.Department{
		Id:             r.ID,
		DepartmentName: r.DepartmentName,
		DepartmentCode: r.DepartmentCode,
		Active:         r.Active,
	}
}

func modelToDepartments(r []*model.Department) []*entity.Department {
	res := make([]*entity.Department, 0, len(r))
	for i := range r {
		res = append(res, modelToDepartment(r[i]))
	}
	return res
}

func (c Repository) CreateDepartment(ctx context.Context, r *entity.Department) (*entity.Department, error) {
	department := &model.Department{
		DepartmentName: r.DepartmentName,
		DepartmentCode: r.DepartmentCode,
		Active:         r.Active,
	}

	_, err := c.db.ModelContext(ctx, department).
		Insert()
	if err != nil {
		log.Println("unable to insert department")
		return nil, status.Error(codes.Internal, "unable to insert department")
	}

	r.Id = department.ID

	return r, nil
}

func (c Repository) UpdateDepartment(ctx context.Context, r *entity.Department) error {
	if !model.IsValidUUID(r.Id) {
		return status.Error(codes.InvalidArgument, "invalid id")
	}

	department := model.Department{
		ID:             r.Id,
		DepartmentName: r.DepartmentName,
		DepartmentCode: r.DepartmentCode,
		Active:         r.Active,
	}

	_, err := c.db.ModelContext(ctx, &department).
		WherePK().
		Update()
	if err != nil {
		log.Println("unable to update department", err)
		return status.Error(codes.InvalidArgument, "unable to update department")
	}

	return nil
}

func (c Repository) GetDepartment(ctx context.Context, id string) (*entity.Department, error) {
	if !model.IsValidUUID(id) {
		return nil, status.Error(codes.InvalidArgument, "invalid id")
	}

	var department model.Department
	err := c.db.ModelContext(ctx, &department).
		Where(model.Columns.Department.ID+" = ?", id).
		Select()
	if err != nil {
		if err == pg.ErrNoRows {
			log.Println("department not found")
			return nil, status.Error(codes.NotFound, "department not found")
		}
		log.Println("unable to select department")
		return nil, status.Error(codes.InvalidArgument, "unable to select department")
	}

	return modelToDepartment(&department), nil
}

func (c Repository) ListDepartments(ctx context.Context) ([]*entity.Department, error) {
	var departments []*model.Department
	err := c.db.ModelContext(ctx, &departments).
		Select()
	if err != nil {
		log.Println("unable to select departments")
		return nil, status.Error(codes.InvalidArgument, "unable to select departments")
	}

	return modelToDepartments(departments), nil
}

func (c Repository) DeleteDepartment(ctx context.Context, id string) error {
	if !model.IsValidUUID(id) {
		return status.Error(codes.InvalidArgument, "invalid id")
	}

	// проставить в null поле r.DepartmentID у всех пользователей
	// и статус проставить неактивный

	_, err := c.db.ModelContext(ctx, (*model.Department)(nil)).
		Where(model.Columns.Department.ID+" = ?", id).
		Delete()
	if err != nil {
		log.Println("unable to delete department")
		return status.Error(codes.Internal, "unable to delete department")
	}

	return nil
}
