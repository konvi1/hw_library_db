package entity

import "time"

type Department struct {
	Id             string
	DepartmentName string
	DepartmentCode string
	Active         bool
}

type Position struct {
	Id           string
	PositionName string
	PositionCode string
}

type User struct {
	Id             string
	UserSurname    string
	UserFirstname  string
	UserSecondname *string
	Birthday       time.Time
	DepartmentID   *string
	PositionID     *string
	Active         bool
}
